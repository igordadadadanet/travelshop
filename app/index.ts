import "./assets/styles/main.scss"
import { ModalService } from "@services/modal/modal.service";
import { getTours } from "@rest/tours";
import { ITour } from "./models/tours/tours";
import { ContentItemService } from "@services/content-item/content-item.service";
import { images } from "@services/img/img";
import { initFooterTitle, initHeaderTitle } from "@services/general/general";

const imagesStore = images;
initHeaderTitle('Туры');
initFooterTitle('Божественные туры');

// function openModal(id: string | null = null): void {
//     const template = '<div>MyModal</div>';
//     const modal = new ModalService(id);
//     modal.open(template);
// }
//
// function openModalSecond(id: string | null = null): void {
//     const template = '<div>MyModal2</div>';
//     const modal = new ModalService(id);
//     modal.open(template);
// }
//
// function removeModal(): void {
//     ModalService.removeById();
// }
//
// function removeAll() {
//     ModalService.removeAll();
// }

// const openModalButton: HTMLButtonElement = document.querySelector('.openModal')!;
// openModalButton.addEventListener('click', () => openModal());
//
// const openModalSecondButton: HTMLButtonElement = document.querySelector('.openModalSecond')!;
// openModalSecondButton.addEventListener('click', () => openModalSecond());
//
// const removeModalButton: HTMLButtonElement = document.querySelector('.removeModal')!;
// removeModalButton.addEventListener('click', () => removeModal());
//
// const removeAllButton: HTMLButtonElement = document.querySelector('.removeAll')!;
// removeAllButton.addEventListener('click', () => removeAll());

export function contentLoaded() {
    const content = document.querySelector('.content');
    if (content) {
        content.innerHTML = '';
    }
}

document.addEventListener('DOMContentLoaded', () => {
    // const goToTours: HTMLButtonElement = document.querySelector('.goToTours')!;
    // goToTours.addEventListener('click', () => window.location.href = 'app/pages/tours/index.html');
    // const goToTickets: HTMLButtonElement = document.querySelector('.goToTickets')!;
    // goToTickets.addEventListener('click', () => window.location.href = 'app/pages/tickets/index.html');

    getTours().then((data: ITour[]) => {
        contentLoaded();
        data.forEach((tour: ITour) => {
            new ContentItemService(tour).createItem();
        })
    });
})