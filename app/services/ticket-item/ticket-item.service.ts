import { ITour } from "../../models/tours/tours";
import { ticketItemTemplate } from "../../templates/ticket-item";
import { postTicketData } from "@rest/ticket";
import { TicketType } from "../../models/ticket/ticket";

export function initTicketInfo(item: ITour | TicketType): void {
    const contentContainer = document.querySelector('.content') as HTMLElement;
    contentContainer.innerHTML = '';
    contentContainer.innerHTML = ticketItemTemplate(item);
 }

function initUserData() {
    const userInfo = document.querySelectorAll('.user-info > p');
    let userInfoObj: any;
    userInfo.forEach((el) => {
        const inputDataName = el.getAttribute('data-name');
        if (inputDataName && 'inputDataName' in userInfoObj) {
            const inputElems = el.querySelector('input')!;
            userInfoObj[inputDataName] = inputElems.value;
        }
    });

    console.log('userInfoObj',userInfoObj)
    return userInfoObj;
}

function initPostData(data: any) {
    initUserData();
    postTicketData(data).then((data) => {
        if (data.success) {

        }
    })
}
let ticketPostInstance: any;
function registerConfirmButton(): void {
    const targetEl = document.getElementById('accept-order-button');
    if (targetEl) {
        targetEl.addEventListener('click', () => {
            initPostData(ticketPostInstance);
        });
    }
}
