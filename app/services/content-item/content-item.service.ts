import { ITour } from "../../models/tours/tours";
import { ModalService } from "@services/modal/modal.service";
import { tourItemTemplate } from "../../templates/tour-item";

export class ContentItemService {
    public static items: ContentItemService[] = [];
    constructor(public item: ITour) {
        ContentItemService.items.push(this);
    }

    createItem() {
        const item: HTMLElement = document.createElement('div');
        item.classList.add('col-xl-3', 'col-6');
        item.innerHTML = tourItemTemplate(this.item);
        item.addEventListener('click', () => {
            new ModalService(this.item).open();
        })
        this.appendToParent(item);
    }

    private appendToParent(child: HTMLElement) {
        const parentContainer = document.querySelector('.content')!;
        if (parentContainer) {
            parentContainer.appendChild(child);
        }
    }
}