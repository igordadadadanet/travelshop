
/* Общие методы используются для вставки текста в header   footer*/

/*  -
    - Указать в методах возвращающие типы, типы для параметров, в теле функции также указать типы
*/
export function initHeaderTitle(ticketName: string): void {
    const headerElement= document.querySelector('header') as HTMLElement;
    const targetItem = headerElement.querySelector('h2') as HTMLHeadElement;
    if (targetItem) {
        targetItem.innerText = ticketName;
    }
}

export function initFooterTitle(ticketName: string): void {
    const headerElement = document.querySelector('footer') as HTMLElement;
    const targetItem = headerElement.querySelector('h3') as HTMLHeadElement;
    if (targetItem) {
        targetItem.innerText = ticketName;
    }
}