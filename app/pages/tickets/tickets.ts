import { ITour } from "../../models/tours/tours";
import '@myCss';
import { initFooterTitle, initHeaderTitle } from "@services/general/general";
import { initTicketInfo } from "@services/ticket-item/ticket-item.service";
import { getTicketById } from "@rest/ticket";
import { TicketType } from "../../models/ticket/ticket";


document.addEventListener('DOMContentLoaded', () => {
    // const goToTours: HTMLButtonElement = document.querySelector('.goToTours')!;
    // goToTours.addEventListener('click', () => window.location.href = 'app/pages/tours/index.html');
    // const goToTickets: HTMLButtonElement = document.querySelector('.goToTickets')!;
    // goToTickets.addEventListener('click', () => window.location.href = 'app/pages/tickets/index.html');
    let id = '';
    const idIdx = window.location.search.search('id=');
    if (idIdx !== -1) {
        id = window.location.search.substring(idIdx + 3);
    }
    getTicketById<TicketType>(id).then((data: TicketType[]) => {
        // let finding = data.find((item: TicketType) => {
        //     return +item.id === +id;
        // })
        // finding = finding ? finding : data[0];
        const finding = data[0];
        const ticketName = typeof finding?.name === "string" ? finding?.name : '';
        initHeaderTitle(ticketName);
        initFooterTitle('Туры по всему миру');
        initTicketInfo(finding);
    });
})

